/******************************************************************************************************
* Student Name: Brodie Taillefer
* Student Number: 040 757 711
* Assignment 1 - Floatin-Point Converter
* Course Name: CST 8233 - Numerical Computing
* Lab Section: 302
* Professor: Andrew Tyler
* Due Date: February 16, 2015
* Submission Date: February 13, 2015
* Source Files: ass1.c
*******************************************************************************************************/

/*******************************************************************************************************
* File Name: ass1.c
* Version: 1.0
* Author: Brodie Taillefer
* Purpose: To convert a floating and double value into it's in memory representation. This shows that the values aren't exactly
*		   the value you enter. With ass1.c we are able to view the hex and binary representation in memory of float and double values
*******************************************************************************************************/
#include <stdlib.h>
#include <stdio.h>

void convertEndian(int size, unsigned char * num);
void printFloat();
void printDouble();
void printHex(size_t size, unsigned char *pNum);
void printBinary(size_t size, unsigned char *pNum);

/********************************************************************************************************
* Function Name: main
* Purpose: Main program file for ass1.c, allows the user to choose what methods they wish to call, either 
*          convert a float, double or quit.
* Function In parameters: void
* Function out parameters: int
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
int main(void) {
	int response;
	int running = 0;

	while (!running) {
		printf("1 = convert a float\n");
		printf("2 = convert a double\n");
		printf("3 - quit\n");

		scanf_s("%d", &response);
		switch (response) {
		case 1:
			printFloat();
			break;
		case 2:
			printDouble();
			break;
		case 3:
			running = 1;
			break;
		}
	}
	return 0;
}

/********************************************************************************************************
* Function Name: printFloat
* Purpose: Allows the user to convert and show a float's value memory representation in both binary and HEX.
* Function In parameters: void
* Function out parameters: none
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void printFloat() {
	float floatNum = 0;
	unsigned char *pFloat = (unsigned char*)&floatNum; //Pointer to floatNum
	printf("Please enter the float number to convert: ");
	scanf_s("%f", &floatNum);
	fflush(stdin);
	printf("Float number is:\t%.6f\n", floatNum); //Print the float number the user entered
	convertEndian(sizeof(float),(unsigned char*)&floatNum); //Convert to Big Endian
	printf("Binary:\t\t\t");
	printBinary(sizeof(float),pFloat); //Print the float's binary representation
	printf("\nHex Big Endian: \t");
	printHex(sizeof(float),pFloat); //Print the float's HEX representation in BIG Endian
	convertEndian(sizeof(float), (unsigned char *)&floatNum); //Convert back to little endian
	printf("Hex Little Endian: \t");
	printHex(sizeof(float),pFloat); //Print the float's HEX representation in LITTLE Endian
}
/********************************************************************************************************
* Function Name: printDouble
* Purpose: Allows the user to convert and show a float's value memory representation in both binary and HEX.
* Function In parameters: void
* Function out parameters: int
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void printDouble() {
	double doubleNum = 0;
	unsigned char *pDouble = (unsigned char *)&doubleNum; //Pointer to doubleNum
	printf("Plese enter the double number to convert: ");
	scanf_s("%lf", &doubleNum);
	fflush(stdin);
	printf("Double number is:\t%.6lf\n", doubleNum); //Print the double number the user entered
	convertEndian(sizeof(double), (unsigned char*)&doubleNum); // Convert to Big Endian
	printf("Binary:\n");
	printBinary(sizeof(double),pDouble);//Print the double's binary representation
	printf("\nHex Big Endian: \t");
	printHex(sizeof(double),pDouble); //Print the double's HEX representation in BIG Endian
	convertEndian(sizeof(double), (unsigned char *)&doubleNum); //Convert to Little Endian
	printf("Hex Little Endian: \t");
	printHex(sizeof(double),pDouble); //Print the double's HEX representation in LITTLE Endian
}
/********************************************************************************************************
* Function Name: converyEndian
* Purpose: Swaps the number location in memory to the opposite endian, competely swapping upto the size of the data type in memory
* Function In parameters: size of data type, unsigned char * to the number in memory
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void convertEndian(int size, unsigned char * pNum) {
	char i;
	char* tempLocation = (char *)malloc(size); //Allocate a new location in memory to place the reversed bits
	for (i = 0; i < size; i++) { //
		tempLocation[i] = pNum[size - (i+1)]; //Bit by bit, the last location in the number will now be first in tempLocation
	}
	for (i = 0; i < size; ++i){
		pNum[i] = tempLocation[i]; // Seed back the reversed number
	}
}
/********************************************************************************************************
* Function Name: printHex
* Purpose: Prints out the number's hex value in memory
* Function In parameters: size of the data type, unsigned char * to the number in memory
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void printHex(size_t size, unsigned char *pNum) {
	unsigned int i=0;
	for (i = 0; i < size; i++) { // Loop through the size of the number, printing out 2 HEX values(8 bits)
		printf("%.2X ", pNum[i]);
	}
	printf("\n");
}

/********************************************************************************************************
* Function Name: printBinary
* Purpose: Print out the number's binary value in memory
* Function In parameters: size of the data type, unsigned char * to the number in memory
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void printBinary(size_t size, unsigned char *pNum) {
	unsigned char i = 0;
	unsigned char j;
	for (i = 0; i < size; i++) { //Loop for the size of the data type
		for (j = 128; j > 0; j >>= 1) { //Right shift bit by bit to to check every bit, 1000000,01000000 etc
			((pNum[i] & j))? printf("1") : printf("0"); //Binary AND to see if the current bit in memory is set
			if (j == 16 || j == 1) { //Need a space between every 4th set of bits, 128 >> 4 and 128 >> 8
				printf(" ");
			}
		}
	}
}
