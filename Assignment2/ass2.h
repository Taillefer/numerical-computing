/********************************************************************************************************
* File Name: ass2.h
* Purpose: Header file for ass2, includes the constant for max aninmals that can be entered, the struct to define an animal and the
*		   function prototypes.
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/

#define MAX_ANIMALS 100

typedef struct {
	char *name;
	float body_weight;
	float brain_weight;
}animal;

void printAnimals(animal*[]);
void interpolate(double,double,double,double,animal*[]);
void extrapolate(double, double,double,double);
void cleanup(animal*[]);
