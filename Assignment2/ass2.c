/*************************************************************************************************
* File Name: ass2.c
* Version: V1.0
* Author: Brodie Taillefer
* Student Number: 040 757 711
* Course Name: CST 8233 Numerical Computing
* Lab Section: 302
* Assignment Number: 2
* Assignment Name: Linear Regression
* Due Date: March 16, 2015
* Submission Date: March 16, 2015
* Professor's Name: Andrew Tyler
* Purpose: Use square of sum of residuals to calculate the estimated brain weight of an animal
* Source Files: ass2.c ass2.h brains.txt
********************************************************************************************************/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ass2.h"
#include <malloc.h>
#include <float.h>

/********************************************************************************************************
* Function Name: main
* Purpose: Main program file for ass2.c defines variables and opens the supporting file, loading all the data
*		   into an array of pointers to animals, calculating the smallest and largest animal and calculating the sum of
*		   squares fo residuals.
* Function In parameters: void
* Function out parameters: int
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
int main() {
	char file_name[25]; //Variable to hold user requested file name
	char c;
	animal* animals[MAX_ANIMALS]; //Array of pointers to struct animal
	double sumx = 0,sumy = 0,sumxy = 0,sumxsquare = 0,largestWeight = 0, m = 0, b = 0;
	int choice = 0,counter = 0,i = 0;
	double smallestWeight = DBL_MAX; //Set the smallestWeight to largest value, so every value afterwards will be valid smallest
	char word[100];
	FILE * file;
	for(i=0;i<MAX_ANIMALS;i++) { //Set all the pointers to 0
		animals[i] = NULL;
	}
	printf("OPEN FILE\n");
	printf("Please enter the name of the file to open: \n");
	scanf("%s",file_name); //Request file_name
	file = fopen(file_name,"r"); //Open the file in read only mode
	if(!file) { //If we couldn't grab the file, error out
		printf("Error while opening the requested file\n");
		exit(EXIT_FAILURE);
	}
	printf("READING RECORDS INTO MEMORY\n");
	i = 0;
	do { //Loop to read the file
		c= fscanf(file,"%s",word); //Read word by word, 
		if(c == EOF) { //If we read the end of file, stop readding
			break;
		}
		animals[i] = (animal *)malloc(sizeof(animal)); //Malloc location for a new animal struct
		animals[i]->name = (char *)malloc(strlen(word)+1); //Malloc location for the animal name
		strcpy(animals[i]->name,word); //strcpy to pointer
		c= fscanf(file,"%s",word); //Read next word
		animals[i]->brain_weight = (float)strtod(word,NULL); //Convert string to float
		c= fscanf(file,"%s",word); //Read next word
		animals[i]->body_weight = (float)strtod(word,NULL); //Convert string to float
		if(animals[i]->body_weight > largestWeight) { //Check to see if the current animal is the largest we've seen
			largestWeight = animals[i]->body_weight;
		}
		if(animals[i]->body_weight < smallestWeight) { //Check to see if the current animal is the smallest we've seen
			smallestWeight = animals[i]->body_weight;
		}
		sumx+=animals[i]->body_weight; //Take the body weight and add it to sum of x
		sumy+=animals[i]->brain_weight; //Take the brain weight and add it to sum of y
		sumxy+=(animals[i]->body_weight * animals[i]->brain_weight); //Take the body weight and brain weight and add it to sum xy
		sumxsquare+= (animals[i]->body_weight * animals[i]->body_weight); //Take the body Weight and add it's square to sumxsquare
		i++;
	}while(c != EOF);

	m = ((i * sumxy) - (sumx * sumy)) / ((i * sumxsquare) - (sumx * sumx)); //Calculate the m for sum of squares
	b = ((sumxsquare * sumy)  - (sumxy * sumx)) / ((i * sumxsquare) - (sumx * sumx)); //Calculate the b for sum of squares
	printf("records read: %d\n",i);
	while(choice != 4){ //User choice menu
		printf("1. List records in memory\n");
		printf("2. Interpolate\n");
		printf("3. Extrapolate\n");
		printf("4. Quit\n");

		scanf("%d",&choice);

		switch(choice) {
			case 1:
				printAnimals(animals); //Print the current animals loaded
				break;
			case 2:
				interpolate(smallestWeight,largestWeight,m,b,animals); //Interpolate the data
				break;
			case 3:
				extrapolate(smallestWeight,largestWeight,m,b); //Extrapolate the data
				break;
			case 4:
				cleanup(animals); //Exit the application, clear all dynamic memory
				break;
		}
	}
}
/********************************************************************************************************
* Function Name: printAnimals
* Purpose: Print out all the animals that are currently loaded into the array of pointers to animals
* Function In parameters: pointer to array of animals
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void printAnimals(animal *animals[]) {
	int i = 0;
	printf("LIST RECORDS IN MEMORY\n");
	for(i=0;i<MAX_ANIMALS;i++) { 
		if(animals[i]) { //If the pointer to animal exists
			printf("Animal #%d\n",i+1);
			printf("Name = %s\n",animals[i]->name);
			printf("Body Weight = %.2f kilograms\n",animals[i]->body_weight);
			printf("Brain Weight = %.2f grams\n",animals[i]->brain_weight);
			printf("\n");
		}
		else {
			break;
		}
	}
}

/********************************************************************************************************
* Function Name: interpolate
* Purpose: Interpolate the data given a user weight
* Function In parameters: double the smallest animal body weight, double the largest animal body weight, double sum of squares x, sum of squares b , pointer to array of animals
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void interpolate(double smallestWeight, double largestWeight,double m, double b,animal *animals[]) {
	double x = 0;
	double y = 0;
	int i = 0;
	double closestWeight = DBL_MAX;
	int animalNumber = 0;
	double weightDifference = 0;
	printf("The body weight must be in the range %lf kg to %lf kg\n",smallestWeight,largestWeight);
	printf("Least squares fit is:\n\t\t brain_weight(grams) = %lf * body_weight(kg) + %lf\n",m,b);
	printf("Please enter a body weight(kg) to interpolate at.\n");
	printf("Must be in the range of %lf kg to %lf kg: \n",smallestWeight,largestWeight);
	scanf("%lf",&x); //Get the user input weight
	while(x < smallestWeight || x > largestWeight) { //If user choose's a out of range number, re ask for weight
		printf("error: Please enter within the valid range: \n");
		scanf("%lf",&x);
	}
	y = (m*x) + b; //Calculate the sum of squares given the weight
	printf("Interpolated brain weight is: %.2lf\n",y);
	for(i=0;i<MAX_ANIMALS;i++) { //Find the closest animal with that body weight
		if(animals[i]) {
			weightDifference = animals[i]->body_weight - x;
			if(weightDifference < 0) {
				weightDifference = weightDifference*-1;
			}
			if(weightDifference <closestWeight) {
				closestWeight =  weightDifference;
				animalNumber = i;
			}
		}
		else {
			break;
		}
	}
	printf("The closest animal is: %s\n",animals[animalNumber]->name);
	printf("It is number %d in the list\n",animalNumber+1);
}

/********************************************************************************************************
* Function Name: extrapolate
* Purpose: extrapolate for the user inputed weight
* Function In parameters: double smallest animal weight, double largest animal weight, double sum of squares m, double sum of squares b
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void extrapolate(double smallestWeight, double largestWeight, double m, double b) {
	double x = 0;
	double y = 0;
	printf("The body weight must be outside the range %lf kg to %lf kg\n",smallestWeight,largestWeight);
	printf("Least squares fit is:\n\t\t brain_weight(grams) = %lf * body_weight(kg) + %lf\n",m,b);
	printf("Please enter a body weight(kg) to extrapolate at.\n");
	printf("Must be outside the range of %lf kg to %lf kg: \n",smallestWeight,largestWeight);
	scanf("%lf",&x); //Get user input
	while(x > smallestWeight && x < largestWeight) { //If user input is within weight range, reask for a valid weight
		printf("error: Please enter within the valid range: \n");
		scanf("%lf",&x);
	}
	y = (m*x) + b; //Calculate the brain weight with sum of squares formula
	printf("Extrapolated brain weight is: %.2lf\n",y);
}

/********************************************************************************************************
* Function Name: cleanup
* Purpose: Free all dynamic memory allocated tot hea 
* Function In parameters: pointer to array of animals
* Function out parameters: void
* Version: 1.0
* Author: Brodie Taillefer
*********************************************************************************************************/
void cleanup(animal* animals[]) {
	int i = 0;
	for(i=0;i<MAX_ANIMALS;i++) {
		if(animals[i]) {
			free(animals[i]->name);
			free(animals[i]);
		}
		else {
			break;
		}
	}
}




