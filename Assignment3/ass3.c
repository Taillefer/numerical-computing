/*******************************************************************************************
* FileName: ass3.c
* Version: V1.0
* Author: Brodie Taillefer 
* Student Number: 040 757 711
* Course Name/Number: Numerical Computing CST 8233
* Lab Section: 012
* Assignment Number: Assignment 3
* Assignment Name: Dynamic Population
* Due Date: April 13, 2015
* Submission Date: April 12, 2015
* Professor's Name: Andrew Tyler
* Purpose: Use euler's formula to calculate the rate of change and population of the kentucky deer population. Allow's the user to enter
*          date for various variables, and the ability to adjust them during run-time.
*******************************************************************************************/
#define _CRT_SECURE_NO_WARNINGS /*Prevent unsecure warnings */
#include <time.h>
#include <stdio.h>
#include <conio.h>
#define MS_MIN 60000 /*Miliseconds in a minute*/
extern void simulate(); /*Extern declaration for our simulation function */

/****************************************************************************************
* Function Name: main(void)
* Purpose: Main program for ass3, allows user to choose either to run the simulation or quit
* Function in parameters: void
* Function out parameters: int
* Version: V1.0
* Author: Brodie Taillefer
****************************************************************************************/
int main(void) {
	int userchoice = 0;
	/*While the user does not choose to Quit, continue looping the menu*/
	while (userchoice != 2) {
		printf("Population Simulation\n");
		printf("1. Run the simulation\n");
		printf("2. Quit\n");
		scanf("%d", &userchoice);
		switch (userchoice) {
			/*Start the simulation*/
		case 1:
			simulate();
			break;
			/*Quit the program*/
		case 2:
			break;
		}
	}
}

/****************************************************************************************
* Function Name: simulate()
* Purpose: Begin the simulation for the program, allow's the user to enter various variables that are run against euler's formula.
* Function in parameters: none
* Function out parameters: void(none)
* Version: V1.0
* Author: Brodie Taillefer
****************************************************************************************/
void simulate() {
	double population = 0, maxPopulation = 0, harvestRate = 0, growthRate = 0, populationChange = 0, totalIncrease = 0;
	char userInput = 0;
	int difference = 0, minutes = 0, seconds = 0, miliseconds = 0;
	char key = 0;
	clock_t start, ticks, next, end;
	printf("ENTER SIIMULATION PARAMETERS\n");
	printf("Intial population(typically 900000) ? ");
	scanf("%lf", &population);
	printf("Maximum population the environment can support (typically 1000000) ? ");
	scanf("%lf", &maxPopulation);
	printf("Initial harvesting rate (fraction per year - 0 for no harvesting)? ");
	scanf("%lf", &harvestRate);
	printf("Natural fractional growth population rate (typically 0.2 per year)? ");
	scanf("%lf", &growthRate);
	start = end = clock();
	printf("starting simulation\n");
	/*Continue the loop while user input is not q (QUIT)*/
	while (userInput != 'q') { 
		next = clock();
		/*If the time has changed since the last calculation, we should recalculate our population*/
		if (next != end) {
			/*Display the last key that was pressed, only if a key has been pressed before*/
			if (key) { 
				printf("%c was pressed \n",key);
			}
			/*Formula to calculate the yearly change in population (1 second)*/
			populationChange = ((growthRate*population) * (1 - (population / maxPopulation)) - (harvestRate * population));
			/*Adjust the population increase to a fraction of a year, this is the step*/
			totalIncrease = (populationChange * ((next - end) * 1000.0) / CLOCKS_PER_SEC / 1000.0);
			/*Add to the population the increase*/
			population +=totalIncrease;
			end = clock();
			ticks = clock();
			/*Calculate the difference in time from the start of the program execution*/
			difference = (int)((ticks - start) * 1000.0 / CLOCKS_PER_SEC);
			/*Minutes will be the diffence in ms / ms in a minute (60,000)*/
			minutes = (difference / MS_MIN);
			/*Seconds will be the differnce subtracted by total minutes, divided by ms/sec (1000)*/
			seconds = (difference - (minutes * MS_MIN)) / 1000;
			/*miliseconds will be the the difference substracted by total minutes, by total seconds*/
			miliseconds = difference - ((minutes * MS_MIN) + seconds * 1000);
			printf("Simulation run time minute = %d; second = %d; millisec = %4d\n", minutes, seconds, miliseconds);
			printf("***************************************************************\n");
			printf("Year of simulation = %d\n", seconds + (minutes * 60));
			printf("Rate of population change = %f\n", populationChange);
			printf("Population = %d\n", (int)population);
			printf("****************************************************************\n");
			printf("Press w/e to increase/decrease harvesting rate.\n");
			printf("Current harvesting rate: %.6lf\n", harvestRate);
			printf("Press p/o to increase/decrease Max population support.\n");
			printf("Current Max population: %d\n", (int)maxPopulation);
			printf("Press k/l to increase/decrease growth rate.\n");
			printf("Current growth rate: %f\n", growthRate);
			printf("Press q to quit\n");
			/*If a key has been hit, we wish to increase/decrease the constant formula values*/
			if (_kbhit()) {
				/*Get the key that was pressed*/
				key = _getch();
				/*Switch statement to decide what to do based on the key that was pressed*/
				switch (key) {
					/*If w increase the harvest rate*/
				case 'w':
					harvestRate += 0.01;
					break;
					/*If e decrease the harvest rate */
				case 'e':
					if (harvestRate >= 0.01) {
						harvestRate -= 0.01;
					}
					break;
					/*If p increase the max population*/
				case 'p':
					maxPopulation += 10000;
					break;
					/*If o decrease the max population*/
				case 'o':
					if (maxPopulation >= 10000) {
						maxPopulation -= 10000;
					}
					break;
					/*If k increase the growth rate*/
				case 'k':
					growthRate += 0.01;
					break;
					/*If l decrease the growth rate*/
				case 'l':
					if (growthRate >= 0.01) {
						growthRate -= 0.01;
					}
					break;
					/*If q we should quit the simulation*/
				case 'q':
					userInput = 'q';
					break;
					/*If if was some random key, we really don't care about it */
				default:
					break;
				}
			}
			/*Clear the screen for next calculation*/
			system("cls");
		}
	}
}
